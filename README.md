# Introduction to Ciel and the Arch Linux MIPS r6 Build System

While Arch Linux MIPS r6 is based on Arch Linux for x64 systems, there are some significant differences compared to the upstream. Therefore, before contributing to the Arch Linux MIPS r6 project, one should have some basic understanding with how Arch Linux does things, such as the [Arch Build System](https://wiki.archlinux.org/title/Arch_Build_System), and tools that we use, such as [Ciel](https://github.com/AOSC-Dev/ciel-rs/).


## Overview of Differences

- The `PKGBUILD` tree of the official Arch Linux distribution is organized using both Git and Subversion, we only use Git.
- While both upstream Arch Linux and us enphasize on [building in a clean chroot](https://wiki.archlinux.org/title/DeveloperWiki:Building_in_a_clean_chroot), Arch Linux does not have a standard way of setting up clean chroots. We use the Ciel container manager (originally developed for AOSC OS) to quickly provision and dispose containers and use custom Ciel plugins to better integrate with our adapted Arch Build System.
- Certain packages have been renamed. For example, `archlinux-keyring` (the package that ships package signing keys) have been renamed to `almipsr6-keyring` to avoid confusion with upstream.
- Due to the lack of actual MIPS r6 silicon, we are using QEMU user-mode emulation in conjunction with Linux kernel's `binfmt` feature to spin up emulated containers running MIPS r6 binaries on x86 or ARM hardware.
- We currently have an additional `upstream` repository, that contains binary packages directly imported from upstream (must have architecture `any`).
- Because MIPS r6 architectures are relatively new, we try to specify the GNU triplet `mipsisa64r6el-unknown-linux-gnuabi64` manually and [autoreconf](https://wiki.debian.org/Autoreconf) whenever possible (when building packages that use the GNU Build System).
- For packages built using CMake, the Ninja backend is preferred over UNIX `make` due to speed advantages on emulated build environments.
- Currently we only build a pure MIPS64 system, and multilib support has been intentionally turned off.
- Currently LTO is mostly disabled due to bugs with upstream toolchains.
- While we try to make as little changes to the upstream as possible, some adaptive changes (such as build-time switches, compiler flags, etc.) have been modified to make it work with our architecture.


## Things to Know Before You Begin

- [How to Ask Questions the Smart Way](http://www.catb.org/~esr/faqs/smart-questions.html)
- [Linux from Scratch](https://www.linuxfromscratch.org/)
- [Pacman](https://wiki.archlinux.org/title/Pacman)
- [Package signing](https://wiki.archlinux.org/title/Pacman/Package_signing)
- [Creating packages](https://wiki.archlinux.org/title/Creating_packages)
- [Makepkg](https://wiki.archlinux.org/title/Makepkg)
- [Patching packages](https://wiki.archlinux.org/title/Patching_packages)
- [systemd-nspawn](https://wiki.archlinux.org/title/Systemd-nspawn)
- [QEMU](https://wiki.archlinux.org/title/QEMU)
- Source code of Ciel and our Ciel plugins (Python version), especially the comments
- The `PKGBUILD` tree of [Arch RISC-V](https://github.com/felixonmars/archriscv-packages) project


## Asking for Help
- You need to be confortable with reading and writing in English as most open source communities use English as the standard workplace language.
- Start with a web search for known issues.
- If the issue you are encountering is related to Ciel, ask for help from AOSC OS community.
- If the issue you are encountering is related to Arch itself, ask for help on the Arch Linux mailing lists. But beware that there are some elitism within the Arch community. Therefore, be sure to read the *How to Ask Questions the Smart Way* and be prepared.
- If the issue is encountering is related to upstream software's MIPS support, your mentor or colleagues are the best resources.

Happy porting!
