# Introduction to Ciel and the Arch Linux MIPS r6 Build System

虽然 Arch Linux MIPS r6 在构建过程中尽可能与官方 Arch Linux 保持一致，但是在实践中本项目根据实际需要依然做出了一些改动。因此，在参与本项目前，贡献者需要对诸如 [Arch Build System](https://wiki.archlinux.org/title/Arch_Build_System) 之类的上游项目相关概念与工具以及 [Ciel](https://github.com/AOSC-Dev/ciel-rs/) 等本项目所采用的工具均有所理解。


## Overview of Differences

- 上游的 `PKGBUILD` 同时使用 Git 以及 Subversion 进行管理，而本项目仅使用 Git.
- 虽然上游 Arch 与本项目均强调[在干净环境内构建](https://wiki.archlinux.org/title/DeveloperWiki:Building_in_a_clean_chroot)，但是上游并未提供统一的干净环境配置方式。本项目采用由 AOSC OS 开发的 Ciel 容器管理环境来配置与善后干净的容器环境，并采用一系列插件来增强 Ciel 与 Arch Linux 相关构建系统的整合。
- 为避免混淆，本项目重命名了诸如 `archlinux-keyring`（包含软件包签名密钥）之类的软件包。
- 由于缺乏 MIPS r6 芯片，本项目目前采取了通过 Linux 的 `binfmt` 特性自动调用 QEMU 以运行异构容器进行构建的方案。
- 本项目额外增加了用于存放直接从上游复用架构无关二进制软件包的 `upstream` 仓库。
- 由于 MIPS r6 架构本身较新，本项目在构建软件包时一般会手动指定目标架构为 `mipsisa64r6el-unknown-linux-gnuabi64` 并在可能时使用 [autoreconf](https://wiki.debian.org/Autoreconf).
- 对于使用 CMake 进行构建的软件包，本项目偏好使用 Ninja 后端以改善构建速度。
- 本项目目前为纯 64 位环境。
- 本项目为避开相关 bug 而刻意关闭了链接时优化。
- 其他 MIPS r6 相关改动（编译器选项等）。


## Things to Know Before You Begin

- [提问的智慧](http://www.catb.org/~esr/faqs/smart-questions.html)
- [Linux from Scratch](https://www.linuxfromscratch.org/)
- [Pacman](https://wiki.archlinux.org/title/Pacman)
- [Package signing](https://wiki.archlinux.org/title/Pacman/Package_signing)
- [Creating packages](https://wiki.archlinux.org/title/Creating_packages)
- [Makepkg](https://wiki.archlinux.org/title/Makepkg)
- [Patching packages](https://wiki.archlinux.org/title/Patching_packages)
- [systemd-nspawn](https://wiki.archlinux.org/title/Systemd-nspawn)
- [QEMU](https://wiki.archlinux.org/title/QEMU)
- Source code of Ciel and our Ciel plugins (Python version), especially the comments
- The `PKGBUILD` tree of [Arch RISC-V](https://github.com/felixonmars/archriscv-packages) project


## Asking for Help
- 由于在开源界英语为最普遍的工作语言，因此需要能流利读写英语。
- 在提问前，先使用搜索引擎寻找已知问题。
- Ciel 相关问题请向 AOSC OS 社区提问。
- Arch 相关问题请直接向 Arch 社区提出。由于 Arch 社区内的老派开源社区风气，请先熟读《提问的智慧》。
- 如果遇到软件 MIPS 支持相关问题，请直接向同事与导师求助。

预祝移植愉快！
